"use strict"

var http = require('http');

http.createServer(function(req,res){
    res.writeHead(200, {"Content-Type": "text/plain"});
    res.end("This is now a paragraph about myself as demanded by the lab notes.");

}).listen(3000);