"use strict"

var express = require('express');
var index = express();
index.set('port', process.env.PORT || 3000);
index.use(express.static (__dirname + '/thebook'));

index.get('/', function (req, res){
    res.type('text/html');
    res.sendFile(__dirname + '/thebook/animatedbook.html');
})

index.use(function (req, res){
    res.type('text/html');
    res.status(404);
    res.send('Page Not Found');
});

index.listen(index.get('port'), function(){
    console.log('Express started on http://localhost:' + index.get('port'));
})